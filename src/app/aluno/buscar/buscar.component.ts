import { Component, OnInit } from '@angular/core';
import { Turma } from '../../shared/modelo/turma';
import { TurmaService } from '../../shared/api/turma.servico';

@Component({
  selector: 'app-buscar',
  templateUrl: './buscar.component.html',
  styleUrls: ['./buscar.component.scss']
})
export class BuscarComponent implements OnInit {
  nomeTurma: String;
  disciplina: String;
  horario: String;
  turmas: Array<Turma> = [];
  colunasTabela = ['turma', 'disciplina', 'horario'];

  constructor(private turmaService: TurmaService) { }

  ngOnInit() {
  }

  public buscarTurmas() {
    this.turmas = this.turmaService.buscarTurmas();
  }

  public limparCampos() {
    this.nomeTurma = null;
    this.disciplina = null;
    this.horario = null;
  }
}
