import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';
import { ResponderInboxComponent } from '../../shared/dialogos/responder-inbox/responder-inbox.component';

@Component({
  selector: 'app-inbox',
  templateUrl: './inbox.component.html',
  styleUrls: ['./inbox.component.scss']
})
export class InboxComponent implements OnInit {
  mostrar1 = false;
  mostrar2 = false;
  mostrar3 = false;

  constructor(public dialog: MatDialog) { }

  ngOnInit() {
  }

  public responder() {
    this.dialog.open(ResponderInboxComponent, {width: '400px'});
  }
}
