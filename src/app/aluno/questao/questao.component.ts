import { Component, OnInit, Input } from '@angular/core';
import { Questao } from '../../shared/modelo/questao';
import { MatSnackBar } from '@angular/material';

@Component({
  selector: 'app-questao',
  templateUrl: './questao.component.html',
  styleUrls: ['./questao.component.scss']
})
export class QuestaoComponent implements OnInit {
  @Input() tipo: String;
  @Input() numero: Number;
  @Input() questao: Questao;
  mostrar = false;

  constructor(public snackBar: MatSnackBar) {}

  ngOnInit() {}

  multiplaEscolha() {
    return this.questao.tipoQuestao === 'Múltipla Escolha';
  }

  discursiva() {
    return this.questao.tipoQuestao === 'Discursiva';
  }

  verdadeiroOuFalso() {
    return this.questao.tipoQuestao === 'Verdadeiro ou Falso';
  }

  public enviarResposta() {
    this.snackBar.open('Resposta enviada!', '', {
      duration: 2000
    });
  }

  public verificarRespostaParaFonte() {
    if (this.questao.certa !== null) {
      if (this.questao.certa) {
        return 'green';
      } else {
        return 'red';
      }
    }
    return 'black';
  }
}
