import { Questao } from './../../shared/modelo/questao';
import { SalaService } from './../../shared/api/sala.servico';
import { Component, OnInit } from '@angular/core';
import { MatSnackBar, MatTabChangeEvent } from '@angular/material';

@Component({
  selector: 'app-sala',
  templateUrl: './sala.component.html',
  styleUrls: ['./sala.component.scss']
})
export class SalaComponent implements OnInit {
  introducao: String;
  turma: String;
  perguntasDaTurma: Array<Questao> = [];
  perguntasExibidas: Array<Questao> = [];

  constructor(private salaService: SalaService, public snackBar: MatSnackBar) {}

  ngOnInit() {
    this.turma = this.salaService.buscarTurma();
    this.introducao = this.salaService.buscarIntroducao();
  }

  public buscarPerguntas() {
    this.perguntasDaTurma = this.salaService.buscarPerguntas();
    this.perguntasExibidas = this.perguntasDaTurma;
  }

  public filtrarLidas() {
    this.perguntasExibidas = this.perguntasDaTurma.filter(
      pergunta => pergunta.lida === true
    );
  }

  public filtrarRespondidas() {
    this.perguntasExibidas = this.perguntasDaTurma.filter(
      pergunta => pergunta.respondida === true
    );
  }

  public filtrarCertas() {
    this.perguntasExibidas = this.perguntasDaTurma.filter(
      pergunta => pergunta.certa === true
    );
  }

  public filtrarErradas() {
    this.perguntasExibidas = this.perguntasDaTurma.filter(
      pergunta => pergunta.certa === false
    );
  }

  public limparFiltro() {
    this.perguntasExibidas = this.perguntasDaTurma;
  }

  public enviarPergunta() {
    this.snackBar.open('Pergunta enviada!', '', {
      duration: 2000
    });
  }

  public mudarTab(evento: MatTabChangeEvent) {
    if (evento.tab.textLabel === 'Questões') {
      this.buscarPerguntas();
    }
  }
}
