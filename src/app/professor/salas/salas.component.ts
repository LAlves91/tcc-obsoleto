import { Component, OnInit } from '@angular/core';
import { Turma } from '../../shared/modelo/turma';
import { TurmaService } from '../../shared/api/turma.servico';

@Component({
  selector: 'app-salas',
  templateUrl: './salas.component.html',
  styleUrls: ['./salas.component.scss']
})
export class SalasComponent implements OnInit {
  turmas: Array<Turma>;

  constructor(private turmaService: TurmaService) {}

  ngOnInit() {
    this.turmas = this.turmaService.buscarTurmas();
  }
}
