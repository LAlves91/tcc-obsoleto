import { SalasComponent } from './salas/salas.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MainComponent } from './main/main.component';
import { BuscarComponent } from './buscar/buscar.component';
import { SalaComponent } from './sala/sala.component';
import { QuestaoComponent } from './questao/questao.component';
import { InboxComponent } from './inbox/inbox.component';

const routes: Routes = [
  {
    path: '',
    component: MainComponent,
    children: [
      {
        path: 'salas',
        component: SalasComponent
      },
      {
        path: 'buscar',
        component: BuscarComponent
      },
      {
        path: 'sala',
        component: SalaComponent
      },
      {
        path: 'inbox',
        component: InboxComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProfessorRoutingModule { }
