import { Aluno } from './../../shared/modelo/aluno';
import { AlunoService } from './../../shared/api/aluno.servico';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-buscar',
  templateUrl: './buscar.component.html',
  styleUrls: ['./buscar.component.scss']
})
export class BuscarComponent implements OnInit {
  nomeAluno: String;
  matriculaAluno: String;
  alunos: Array<Aluno> = [];
  colunasTabela = ['matricula', 'nome', 'email', 'curso'];

  constructor(private alunoService: AlunoService) { }

  ngOnInit() {
  }

  public buscarAlunos() {
    this.alunos = this.alunoService.buscarAlunos();
  }

  public limparCampos() {
    this.nomeAluno = null;
    this.matriculaAluno = null;
  }
}
