import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-questao',
  templateUrl: './questao.component.html',
  styleUrls: ['./questao.component.scss']
})
export class QuestaoComponent implements OnInit {
  @Input() tipo: String;
  @Input() numero: Number;

  mostrar = false;
  constructor() { }

  ngOnInit() {
  }

  multiplaEscolha() {
    return this.tipo === 'Múltipla Escolha';
  }

  discursiva() {
    return this.tipo === 'Discursiva';
  }

  verdadeiroOuFalso() {
    return this.tipo === 'Verdadeiro ou Falso';
  }
}
