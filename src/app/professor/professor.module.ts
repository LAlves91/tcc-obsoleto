import { MaterialModule } from './../material/material.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ProfessorRoutingModule } from './professor-routing.module';
import { MainComponent } from './main/main.component';
import { CabecalhoComponent } from './cabecalho/cabecalho.component';
import { MenuLateralComponent } from './menu-lateral/menu-lateral.component';
import { SalasComponent } from './salas/salas.component';
import { SalaComponent } from './sala/sala.component';
import { BuscarComponent } from './buscar/buscar.component';
import { QuestaoComponent } from './questao/questao.component';
import { InboxComponent } from './inbox/inbox.component';

@NgModule({
  imports: [
    CommonModule,
    ProfessorRoutingModule,
    MaterialModule
  ],
  declarations: [MainComponent, CabecalhoComponent, MenuLateralComponent, SalasComponent,
    SalaComponent, BuscarComponent, QuestaoComponent, InboxComponent]
})
export class ProfessorModule { }
