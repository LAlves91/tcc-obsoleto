import { Component, OnInit } from '@angular/core';
import { SalaService } from '../../shared/api/sala.servico';
import { MatSnackBar } from '@angular/material';

@Component({
  selector: 'app-sala',
  templateUrl: './sala.component.html',
  styleUrls: ['./sala.component.scss']
})
export class SalaComponent implements OnInit {
  opcaoQuestao = null;
  introducao: String = 'Introdução inicial';
  turma: String;

  constructor(private salaService: SalaService, public snackBar: MatSnackBar) { }

  ngOnInit() {
    this.turma = this.salaService.buscarTurma();
  }

  public salvarDescricao() {
    this.snackBar.open('Descrição salva!', '', {
      duration: 2000
    });
  }

  public enviarPergunta() {
    this.snackBar.open('Questão enviada!', '', {
      duration: 2000
    });
  }

}
