import { ResponderInboxComponent } from './responder-inbox/responder-inbox.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [ResponderInboxComponent]
})
export class DialogosModule { }
