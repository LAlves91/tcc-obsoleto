import { MatSnackBar, MatDialogRef } from '@angular/material';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-responder-inbox',
  templateUrl: './responder-inbox.component.html',
  styleUrls: ['./responder-inbox.component.scss']
})
export class ResponderInboxComponent implements OnInit {
  resposta: String;

  constructor(public dialogRef: MatDialogRef<ResponderInboxComponent>, public snackBar: MatSnackBar) { }

  ngOnInit() {
  }

  public enviarResposta() {
    this.snackBar.open('Resposta enviada!', '', {
      duration: 2000
    });
    this.dialogRef.close();
  }
}
