import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ResponderInboxComponent } from './responder-inbox.component';

describe('ResponderInboxComponent', () => {
  let component: ResponderInboxComponent;
  let fixture: ComponentFixture<ResponderInboxComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ResponderInboxComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ResponderInboxComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
