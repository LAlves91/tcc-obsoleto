export class Turma {
  id: Number;
  disciplina: String;
  nome: String;
  semestre: String;
  descricao: String;
  horario: String;
  local: String;
}
