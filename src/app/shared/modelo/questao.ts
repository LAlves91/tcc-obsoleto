export class Questao {
  lida: Boolean;
  respondida: Boolean;
  certa: Boolean;
  enunciado: String;
  resposta: String;
  opcoes: Array<String>;
  tipoQuestao: String;
}
