import { AlunoService } from './aluno.servico';
import { SalaService } from './sala.servico';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { TurmaService } from './turma.servico';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [],
  providers: [
    TurmaService,
    SalaService,
    AlunoService
  ]
})
export class ApiModule { }
