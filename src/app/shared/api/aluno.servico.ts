import { Aluno } from './../modelo/aluno';
import { Injectable } from '@angular/core';

@Injectable()
export class AlunoService {
  alunos: Array<Aluno> = [
    {
      nome: 'Aluno 1',
      matricula: '11/1111111',
      email: 'aluno1@unb.br',
      curso: 'Engenharia de Computação'
    },
    {
      nome: 'Aluno 2',
      matricula: '22/2222222',
      email: 'aluno2@unb.br',
      curso: 'Engenharia de Redes'
    },
    {
      nome: 'Aluno 3',
      matricula: '33/3333333',
      email: 'aluno3@unb.br',
      curso: 'Ciência da Computação'
    }
  ];

  public buscarAlunos() {
    return this.alunos;
  }
}
