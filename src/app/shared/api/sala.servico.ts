import { Turma } from './../modelo/turma';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Questao } from '../modelo/questao';

@Injectable()
export class SalaService {
  questoes: Array<Questao> = [
    {
      lida: false,
      respondida: false,
      certa: null,
      enunciado: 'Pergunta não lida',
      resposta: null,
      opcoes: [],
      tipoQuestao: 'Discursiva'
    },
    {
      lida: true,
      respondida: false,
      certa: null,
      enunciado: 'Pergunta lida e não respondida',
      resposta: null,
      opcoes: [],
      tipoQuestao: 'Verdadeiro ou Falso'
    },
    {
      lida: true,
      respondida: true,
      certa: true,
      enunciado: 'Pergunta respondida e certa',
      resposta: '1',
      opcoes: [
        'Opção 1',
        'Opção 2',
        'Opção 3',
        'Opção 4'
      ],
      tipoQuestao: 'Múltipla Escolha'
    },
    {
      lida: true,
      respondida: true,
      certa: false,
      enunciado: 'Pergunta respondida e errada',
      resposta: '1',
      opcoes: [],
      tipoQuestao: 'Verdadeiro ou Falso'
    }
  ];

  public buscarIntroducao() {
    return 'Texto Introdutório à matéria.';
  }

  public buscarTurma() {
    return 'Engenharia de Software - Turma A';
  }

  public buscarPerguntas() {
    return this.questoes;
  }
}
