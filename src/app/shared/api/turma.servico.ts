import { Turma } from './../modelo/turma';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';


@Injectable()
export class TurmaService {

  public buscarTurmas()  {
    const turmas = new Array<Turma>();
    const turma1 = new Turma();
    turma1.nome = 'Engenharia de Software - Turma A';
    turma1.horario = '10:00 - 12:00';
    turma1.disciplina = 'Engenharia de Software';
    turmas.push(turma1);
    const turma2 = new Turma();
    turma2.nome = 'Eletrônica - Turma C';
    turma2.horario = '14:00 - 16:00';
    turma2.disciplina = 'Eletrônica';
    turmas.push(turma2);
    const turma3 = new Turma();
    turma3.nome = 'Programação Orientada a Objetos - Turma D';
    turma3.horario = '18:00 - 20:00';
    turma3.disciplina = 'Programação Orientada a Objetos';
    turmas.push(turma3);

    return turmas;
  }
}
