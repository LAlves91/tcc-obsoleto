import { ApiModule } from './shared/api/api.module';
import { ProfessorModule } from './professor/professor.module';
import { AlunoModule } from './aluno/aluno.module';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';

import { AppComponent } from './app.component';
import { MaterialModule } from './material/material.module';
import { ResponderInboxComponent } from './shared/dialogos/responder-inbox/responder-inbox.component';


@NgModule({
  declarations: [
    AppComponent,
    ResponderInboxComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    MaterialModule,
    AlunoModule,
    ProfessorModule,
    ApiModule
  ],
  providers: [],
  entryComponents: [ResponderInboxComponent],
  bootstrap: [AppComponent]
})
export class AppModule { }
